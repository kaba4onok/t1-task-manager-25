package ru.t1.rleonov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M> {

    int getSize();

    @Nullable
    M add(@NotNull M model);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable Comparator comparator);

    boolean existsById(@NotNull String id);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

    @Nullable
    M remove(@NotNull M model);

    @Nullable
    M removeById (@NotNull String id);

    @Nullable
    M removeByIndex (@NotNull Integer index);

    void clear();

}
