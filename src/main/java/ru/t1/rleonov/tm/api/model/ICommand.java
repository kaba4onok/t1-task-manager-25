package ru.t1.rleonov.tm.api.model;

import org.jetbrains.annotations.Nullable;

public interface ICommand {

    @Nullable
    String getArgument();

    String getDescription();

    String getName();

    void execute();

}
